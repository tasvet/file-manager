<?php

namespace Tasvet\FileManager;

class FileManager
{
    public function greet(String $sName)
    {
        return 'Hi ' . $sName . '! How are you doing today?';
    }
}
